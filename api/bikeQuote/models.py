# DB password: inno123
from django.db import models

# Create your models here.

class Insurers(models.Model):
	name = models.CharField(max_length=50)
	feeTable = models.TextField()

	def __str__(self):
		return self.name

class UserAdmin(models.Model):
	name = models.CharField(max_length=50)
	refresh_token = models.CharField(max_length=255)

	def __str__(self):
		return self.name
