import json
import os
import jwt
import base64
import datetime

from django.http import JsonResponse
from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt
from django.core import serializers
from rest_framework import status
from rest_framework.response import Response

from .models import UserAdmin, Insurers

# global variables
user_secret = None
tokenHeader = None

def __initialize():
	global user_secret
	global tokenHeader

	# Setting jtw secrets
	user_secret = os.getenv("USER_SECRET")
	if(user_secret is None):
		user_secret = '34t2ybfvg8'


	# security overshadow
	tokenHeader = jwt.encode(
		{'nothing': 1},
		'any',
		headers={"alg": "HS512", "typ": "JWT"},
		algorithm='HS512'
	).decode().split('.')[0]

# initialization view
__initialize()



# bike quotation quick Middleware
def authMiddleware(request):
	header = request.META

	global user_secret
	global tokenHeader

	user = None
	token = None

	# reconstruct jwt
	try:
		token = tokenHeader + '.' + base64.b64decode(header['HTTP_AUTHORIZATION'].split(' ')[1].encode()).decode()
	except Exception as err:
		return JsonResponse(
			{"erros": ["token is inconsistent", str(err)]},
			status=status.HTTP_401_UNAUTHORIZED,
			safe=False
		), None

	# validate jwt
	try:
		user = jwt.decode(token, user_secret, algorithms=['HS512'])
	except jwt.ExpiredSignatureError as err:
		# Signature has expired
		return JsonResponse(
			{"erros": ["token expired, refresh it using refresh_token", str(err)]},
			status=status.HTTP_401_UNAUTHORIZED,
			safe=False
		), None
	except Exception as err:
		return JsonResponse(
			{"erros": ["Unknown error", str(err)]},
			status=status.HTTP_401_UNAUTHORIZED,
			safe=False
		), None

	return False, user

####
# GET /bikeQuote/
# for testing propose
####
def index(request):
	if(request.method == 'GET'):
		response = json.dumps([{}])

		return JsonResponse(response, content_type='application/json', safe=False)
	else:
		return JsonResponse({}, status=status.HTTP_501_NOT_IMPLEMENTED, safe=False)

####
# POST /bikeQuote/refresh_token/{id}/{refresh_token}
# returns a new token for the requestor
####
@csrf_exempt
def refresh_token(request, id, token):
	if(request.method == 'POST'):
		# get from user from db
		user = None
		try:
			user = UserAdmin.objects.filter(id=id, refresh_token=token)
		except Exception as err:
			return JsonResponse({"erros": ["user with id '" + str(id) + "' and refresh token '" + str(token) + "' not found", str(err)]}, safe=False)

		# check if any was found
		if(not user):
			return JsonResponse({"erros": ["user with id '" + str(id) + "' and refresh token '" + str(token) + "' not found"]}, safe=False)
		else:
			user = user[0]

		# continue if user exists with this refresh_token
		# generate new token
		payload = {
			'iat': datetime.datetime.utcnow(),
			'exp': datetime.datetime.utcnow() + datetime.timedelta(hours=24),
		}
		headers = {
			"alg": "HS512",
			"typ": "JWT"
		}
		response = {
			'token': base64.b64encode(".".join(jwt.encode(
				payload,
				user_secret,
				headers=headers,
				algorithm='HS512'
			).decode().split('.')[1:]).encode()).decode(),
			'refresh_token': user.refresh_token,
			'description': "add to your request's header: Authorization: Basic _your_new_token_",
		}

		return JsonResponse(response, content_type='application/json', status=status.HTTP_200_OK, safe=False)
	else:
		return JsonResponse({}, status=status.HTTP_501_NOT_IMPLEMENTED, safe=False)

####
# GET /bikeQuote/{price}
# price = xxx,yy
# returns a new token for the requestor
####
def quote(request, price):
	# authenticatoin
	notAuthenticated, user = authMiddleware(request)
	if(notAuthenticated):
		return notAuthenticated

	response = {
		'data': {}
	}

	# GET
	if(request.method == 'GET'):
		# get insurers from db
		insurers = None
		try:
			insurers = Insurers.objects.all()
		except Exception as err:
			insurers = []

		# calculate proposal and generate response
		for insurer in insurers:
			table = json.loads(insurer.feeTable)
			current = len(table) - 1
			# remember:
			# 	table[n][0] = R$
			# 	table[n][1] = % (0 - 100)
			while current >= 0:
				value = table[current]
				if(price >= value[0]):
					response['data'][str(insurer)] = round(price * value[1]/100, 2)
					break

				current -= 1

		return JsonResponse(response, status=status.HTTP_200_OK, safe=False)
	else:
		return JsonResponse({}, status=status.HTTP_403_FORBIDDEN, safe=False)
