from django.contrib import admin
from django.urls import path, include, register_converter

from . import views, converters


register_converter(converters.PriceConverter, 'RS')

urlpatterns = [
	path('refresh_token/<int:id>/<slug:token>', views.refresh_token),
	path('<RS:price>', views.quote),
]
