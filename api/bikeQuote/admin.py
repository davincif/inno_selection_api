from django.contrib import admin
from .models import Insurers, UserAdmin

admin.site.register(Insurers)
admin.site.register(UserAdmin)

# Register your models here.
