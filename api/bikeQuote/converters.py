class PriceConverter:
	regex = '^[0-9]+((,|.)[0-9]{2})?$'

	def to_python(self, value):
		return float(value.replace(',', '.'))

	def to_url(self, value):
		return value
