from django.apps import AppConfig


class BikequoteConfig(AppConfig):
	name = 'bikeQuote'
